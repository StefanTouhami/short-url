<?php

namespace App\Controller;

use App\Entity\Link;
use App\Service\Auth;
use App\Service\Shortcut;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/api", name="api_link_")
 */
class LinkController extends FOSRestController
{
    /**
     * @Rest\Get("/links", name="index")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getLinksAction(Request $request, Auth $auth) {
        $res = [
            'data' => $this->getDoctrine()->getRepository(Link::class)->findBy([
                'user' => $auth->decrypt($request->cookies->get('user-id'))
            ],[
                'createdAt' => 'desc'
            ])
        ];
        /** @var Link $item */
        foreach ($res['data'] as &$item)
            $item->setRedirect($this->generateUrl('view_shortcut', ['shortcut' => $item->getShortcut()], UrlGeneratorInterface::NETWORK_PATH));

        $view = $this->view($res, 200);
        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/link", name="add")
     * @RequestParam(
     *     name="address",
     *     nullable=false,
     *     allowBlank=false,
     *     strict=true,
     * )
     * @RequestParam(
     *     name="shortcut",
     *     nullable=true,
     *     allowBlank=true,
     * )
     */
    public function postLinkAction(ParamFetcher $paramFetcher, Request $request, Shortcut $shortcut, Auth $auth) {
        $paramFetcher->all();
        $link = new Link();
        if (!preg_match("/^((http[s]?|ftp):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?$/", $paramFetcher->get('address')) || preg_match("/^{$_SERVER['SERVER_NAME']}/", $paramFetcher->get('address')))
            return $this->handleView($this->view([], 400));
        $link->setAddress($paramFetcher->get('address'));
        $view = $this->view($link, 200);
        try {
            $link->setShortcut($shortcut->make($paramFetcher->get('shortcut')));
            $link->setUser($auth->decrypt($request->cookies->get('user-id')));
            $em = $this->getDoctrine()->getManager();
            $em->persist($link);
            $em->flush();
        } catch (\Exception $e) {
            $view = $this->view($e->getMessage(), 400);
        }
        return $this->handleView($view);
    }

    /**
     * @Rest\Delete("/link/{id}", name="delete")
     */
    public function deleteLinkAction(int $id, Request $request, Auth $auth) {
        $link = $this->getDoctrine()->getRepository(Link::class)->findOneBy([
            'user' => $auth->decrypt($request->cookies->get('user-id')),
            'id' => $id
        ]);
        if (!empty($link)) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($link);
            $em->flush();
        }
        $view = $this->view([], 200);
        return $this->handleView($view);
    }
}