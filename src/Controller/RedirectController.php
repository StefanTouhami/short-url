<?php

namespace App\Controller;


use App\Entity\Link;
use App\Entity\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class RedirectController extends Controller
{
    /**
     * @Route("/{shortcut}/", name="view_shortcut")
     */
    public function index($shortcut) {
        $link = $this->getDoctrine()->getRepository(Link::class)->findOneBy([
            'shortcut' => $shortcut
        ]);
        if (empty($link))
            throw new NotFoundHttpException();

        $link->addView(new View($_SERVER['HTTP_USER_AGENT']));
        $link->incrementViews();
        $em = $this->getDoctrine()->getManager();
        $em->persist($link);
        $em->flush();

        return $this->redirect($link->getAddress());
    }
}