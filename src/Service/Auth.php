<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;

class Auth
{
    private $serverPassword = "FExk2Rth\,A@nh&Nu8D";

    public function init(Request $request):string
    {
        if(!$request->cookies->has('user-id')) {
            $userAgent = $request->headers->get('User-Agent');
            $cookie = openssl_encrypt(md5(rand(1000000,9999999).$userAgent.uniqid()),"AES-128-ECB",$this->serverPassword);
            setcookie("user-id", $cookie, time()+3600*87600);
        }
        return $this->decrypt($request->cookies->get('user-id'));
    }

    public function decrypt(string $string):string
    {
        return openssl_decrypt($string, "AES-128-ECB", $this->serverPassword);
    }

}