<?php

namespace App\Service;


use App\Entity\Link;
use Doctrine\ORM\EntityManagerInterface;

class Shortcut
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $forbiddenList = [
        //todo
    ];


    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param null|string $str
     * @return string
     * @throws \Exception
     */
    public function make(?string $str):string {
        $att = 0;
        while (!($this->available($str) || $att > 5)) {
            $str = $this->_generate($str);
            ++$att;
        }

        if(empty($str))
            throw new \Exception("Can't generate shortcut");
        return $str;
    }
    public function available(string $str):bool {
        if (strlen($str) < 3)
            return false;
        if(in_array($str, $this->forbiddenList))
            return false;
        $pattern = "/^{$_SERVER['SERVER_NAME']}/";
        if(preg_match($pattern, $str))
            return false;
        $checkDB =  $checkDB = $this->entityManager->getRepository(Link::class)->findBy([
            'shortcut' => $str
        ]);
        if(!empty($checkDB))
            return false;
        return true;
    }

    /**
     * @param null|string $str
     * @return string
     */
    private function _generate(?string $str):string
    {
        if(empty($str))
            return $str = $this->_randomString(rand(6,11));

        $hash = md5($str);
        $startPos = rand(0, strlen($hash)-5);
        $newStr = substr($hash, $startPos, rand(8,10));
        $newStr.=rand(0,9);

        return $newStr;
    }

    /**
     * @param int $length
     * @return string
     */
    private function _randomString(int $length = 10):string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}