<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ViewRepository")
 */
class View
{
    use TimestampableEntity;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $httpAgent;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Link", inversedBy="Views")
     * @ORM\JoinColumn(nullable=false)
     */
    private $link;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHttpAgent(): ?string
    {
        return $this->httpAgent;
    }

    public function setHttpAgent(?string $httpAgent): self
    {
        $this->httpAgent = $httpAgent;

        return $this;
    }

    public function getLink(): ?Link
    {
        return $this->link;
    }

    public function setLink(?Link $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function __construct(string $httpAgent)
    {
        $this->httpAgent = $httpAgent;
        $this->setUpdatedAt(new \DateTime());
        $this->setCreatedAt(new \DateTime());
    }
}
