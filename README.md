#short-url
Tool for shortening web links.

To run follow 3 steps:
- `composer install`
- `yarn install`
- `php bin/console doctrine:database:create`
- `php bin/console doctrine:migrations:migrate`
- `yarn encore dev(production)`
<br>
And the last one:<br>
`php bin/console server:run`