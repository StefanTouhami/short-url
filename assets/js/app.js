require('../css/app.scss');

const $ = require('jquery');
require('bootstrap');

/** Vue **/
import Vue from 'vue'
import VueClipboard from 'vue-clipboard2'

Vue.use(VueClipboard);
import navbar from './component/Navbar'
import home from './component/Home'
new Vue({
    el: '#app',
    components: { navbar, home }
});